pub struct Subscription {
    pub id: i64,
    pub app_id: String,
    pub token: String,
    pub description: String,
    pub topic: String,
}
