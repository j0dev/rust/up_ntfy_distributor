CREATE TABLE "properties" (
    "key"    TEXT NOT NULL,
    "value"  TEXT NOT NULL,
    PRIMARY KEY ("key")
);

CREATE TABLE "subscriptions" (
    "id"           INTEGER NOT NULL,
    "app_id"       TEXT NOT NULL,
    "token"        TEXT NOT NULL,
    "description"  TEXT,
    "topic"        TEXT NOT NULL,
    PRIMARY KEY("id" AUTOINCREMENT)
);

CREATE UNIQUE INDEX sub_token_uniq ON subscriptions(token);
CREATE UNIQUE INDEX sub_topic_uniq ON subscriptions(topic);
