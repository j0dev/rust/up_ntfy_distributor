ALTER TABLE subscriptions 
ADD COLUMN "last_push" INTEGER;

ALTER TABLE subscriptions 
ADD COLUMN "last_register" INTEGER;
