use std::borrow::BorrowMut;

use rusqlite::{params, OptionalExtension};
use rusqlite_migration::{M, Migrations};
use tokio_rusqlite::Connection;

use self::subscription::Subscription;

pub mod subscription;

pub struct UpStore {
    conn: Connection,
    server: String,
    topic_prefix: String,
}

impl UpStore {
    /// List all subscriptions
    pub async fn list_subscriptions(&self) -> Vec<Subscription> {
        let ans = self.conn.call(move |conn| {
            let mut stmt = conn.prepare("
                SELECT
                    id,
                    app_id, token, description,
                    topic
                FROM subscriptions;
            ")?;
            let ans = stmt.query_map([], |row| {
                Ok(Subscription{
                    id: row.get(0)?,
                    app_id: row.get(1)?,
                    token: row.get(2)?,
                    description: row.get(3)?,
                    topic: row.get(4)?,
                })
            })?;

            let mut v: Vec<Subscription> = Vec::new();
            for sub in ans {
                if let Ok(sub) = sub {
                    v.push(sub)
                }
            }

            Ok(v)
        }).await.unwrap();

        ans
    }

    /// Retrieve a subscription from a token
    pub async fn get_subscription_by_token(&self, token: String) -> Option<Subscription> {
        let ans = self.conn.call(move |conn| {
            // prepared statement
            let mut stmt = conn.prepare("
                SELECT
                    id,
                    app_id, token, description,
                    topic
                FROM subscriptions
                WHERE token=?;
            ")?;
            // query first row (optional)
            let ans = stmt.query_row(params![token], |row| {
                Ok(Subscription{
                    id: row.get(0)?,
                    app_id: row.get(1)?,
                    token: row.get(2)?,
                    description: row.get(3)?,
                    topic: row.get(4)?,
                }).optional()
            });
            Ok(ans.unwrap())
        }).await;

        return ans.unwrap()
    }

    /// Retrieve a subscription from a topic
    pub async fn get_subscription_by_topic(&self, topic: String) -> Option<Subscription> {
        let ans = self.conn.call(move |conn| {
            // prepared statement
            let mut stmt = conn.prepare("
                SELECT
                    id,
                    app_id, token, description,
                    topic
                FROM subscriptions
                WHERE topic=?;
            ")?;
            // query first row (optional)
            let ans = stmt.query_row(params![topic], |row| {
                Ok(Subscription{
                    id: row.get(0)?,
                    app_id: row.get(1)?,
                    token: row.get(2)?,
                    description: row.get(3)?,
                    topic: row.get(4)?,
                }).optional()
            });
            Ok(ans.unwrap())
        }).await;

        return ans.unwrap()
    }

    /// Add a subscription
    pub async fn add_subscription(&self, sub: Subscription) {
        let _ans = self.conn.call(move |conn| {
            // prepared statement
            let mut stmt = conn.prepare("
                INSERT INTO subscriptions (
                    app_id, token, description,
                    topic
                ) VALUES (?, ?, ?, ?);
            ")?;
            // Insert row
            let ret = stmt.execute(params![sub.app_id, sub.token, sub.description, sub.topic]);
            Ok(ret.unwrap())
        }).await;
    }

    /// Remove a subscription
    pub async fn remove_subscription(&self, sub: Subscription) {
        let _ans = self.conn.call(move |conn| {
            // prepared statement
            let mut stmt = conn.prepare("
                DELETE FROM subscription WHERE id=?;
            ")?;
            // remove row
            let ret = stmt.execute(params![sub.id]);
            Ok(ret.unwrap())
        }).await;
    }
}

/// Load a datastore from a given path
pub async fn load(db_path: &str) -> UpStore {
    // migrate the storage to the latest version
    migrate(db_path);

    // setup connection
    let mut conn = Connection::open(db_path).await.unwrap();

    // set pragma's
    let _ = conn.call(|conn| {
        conn.pragma_update(None, "foreign_keys", "ON").unwrap();
        conn.pragma_update(None, "journal_mode", "WAL").unwrap();
        Ok(())
    }).await;

    // get some basic vars we need
    let server = get_server(conn.borrow_mut()).await;
    let topic_prefix = get_prefix(&mut conn).await;

    // setup the store helper
    let store = UpStore{
        conn,
        server,
        topic_prefix,
    };

    // return store helper
    store
}

/// Migrate the database to the latest version
fn migrate(db_path: &str) {
    //NOTE: For now we are doing synchronous migrations, in the next version, async migrations will be available

    // temporarily synchronous connection
    let mut conn = rusqlite::Connection::open(db_path).unwrap();
    // migration registration
    let migrations = Migrations::new(vec![
        M::up(include_str!("./migrations/00.sql")),
        M::up(include_str!("./migrations/01.sql")),
    ]);
    // do the actual migration
    migrations.to_latest(&mut conn).unwrap();
    // close the connection
    conn.close().unwrap();
}

/// Retrieve the configured server (or the default one)
async fn get_server(conn: &mut Connection) -> String {
    let ans = conn.call(move |conn| {
        // prepared statement
        let mut stmt = conn.prepare("
            SELECT value FROM properties WHERE key=?;
        ")?;
        // query first row (optional)
        let ans: Option<String> = stmt.query_row(params!["server"], |row| {
            return row.get(0);
        }).optional().unwrap();
        Ok(ans)
    }).await;

    // check if we have a server configured or need to return the default
    if let Some(ans) = ans.unwrap() {
        return ans
    } else {
        // default url
        return "https://ntfy.sh".to_string()
    }
}
/// Retrieve the configured topic prefix
async fn get_prefix(conn: &mut Connection) -> String {
    let ans = conn.call(move |conn| {
        // prepared statement
        let mut stmt = conn.prepare("
            SELECT value FROM properties WHERE key=?;
        ")?;
        // query first row (optional)
        let ans: Option<String> = stmt.query_row(params!["topic_prefix"], |row| {
            return row.get(0);
        }).optional().unwrap();
        Ok(ans)
    }).await;

    // check if we have a prefix configured or return the default
    if let Some(ans) = ans.unwrap() {
        return ans
    } else {
        // default prefix
        return "".to_string()
    }
}
