Linux Ntfy UnifiedPush Distributor
======

This repository contains a [UnifiedPush](https://unifiedpush.org/) Distributor for (desktop) Linux that allows notifications to be pushed trough an [ntfy.sh](https://ntfy.sh/) server (self-hosted or hosted)


## Credits
- [**Rust Example Distributor**](https://gitlab.com/j0dev/rust/rust-unifiedpush-example-distributor)
- [**Notify**](https://github.com/ranfdev/Notify) (by [ranfdev](https://github.com/ranfdev) on github)
